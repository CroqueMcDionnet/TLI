{extends file="layout.tpl"}
{block name=pageTitle}Pathologies | Site Acupuncture{/block}
{block name=mainAside}
    <p>Bonjour, <strong>{$username }</strong>. <a href="logout.php">Déconnexion</a></p>
{/block}
{block name=body}
    <main>
        <h2>Catalogue des pathologies</h2>

        <div class="pathology pathologyHeader">
            <span class="pathologyDescription">Description</span>
            <span class="pathologyMeridian">Meridien</span>
            <span class="pathologyType">Type</span>
        </div>
        {foreach from=$pathologies item=$pathology}
        <div class="pathology">
            <span class="pathologyDescription">{$pathology->getDescription()}</span>
            <span class="pathologyMeridian">{$pathology->getMeridian()->getName()}</span>
            <span class="pathologyType">{$pathology->getType()}</span>
        </div>
        {foreachelse}
        <p>Il n'y a pas de pathologies à afficher.</p>
        {/foreach}
    </main>
{/block}
