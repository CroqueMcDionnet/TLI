<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <title>{block name=pageTitle}Projet TLI | Acupuncture{/block}</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css"/>
</head>
<body>
<header>
    <h1 id="siteTitle">Projet TLI | Acupuncture</h1>
    <hr/>
</header>
<aside id="mainAside">
    {block name=mainAside}{/block}
</aside>
<main>
    {block name=body}{/block}
</main>
<footer>

</footer>
</body>
</html>
