{extends file="layout.tpl"}
{block name=pageTitle}Accueil | Site Acupuncture{/block}
{block name=mainAside}
    {if $loginOk}<p id="loginOkMessage">Connexion réussie.</p>{/if}
    {if $signupOk}<p id="signupOkMessage">Inscription réussie. Vous êtes connecté.</p>{/if}
    {if $disconnectOk}<p id="loginOkMessage">Déconnexion réussie.</p>{/if}
    {if !$authenticated}
        <form action="/dologin.php" method="post">
            <fieldset>
                <legend>Connexion</legend>
                {if $hasLoginError}<p id="loginErrorMessage">{$loginErrorMessage }</p>{/if}
                <label for="loginFormUsername">Nom d'utilisateur :</label><input id="loginFormUsername" type="text" name="loginFormUsername"/><br/>
                <label for="loginFormPassword">Mot de passe :</label><input id="loginFormPassword" type="password" name="loginFormPassword"/><br/>
                <button type="submit">Connexion</button>
            </fieldset>
        </form>
        <form action="/dosignup.php" method="post">
            <fieldset>
                <legend>Inscription</legend>
                {if $hasSignupError}<p id="signupErrorMessage">{$signupErrorMessage }</p>{/if}
                <p id="signupFieldFormatHint">
                    Le nom d'utilisateur doit comporter de 3 à 30 caractères alphanumériques.<br/>
                    Le mot de passe doit comporter de 8 à 255 caractères alphanumériques et/ou spéciaux (,.@%$!?_:;&-).
                </p>
                <label for="signupFormUserame">Nom d'utilisateur :</label><input id="signupFormUsername" type="text" name="signupFormUsername"/><br/>
                <label for="signupFormPassword">Mot de passe :</label><input id="signupFormPassword" type="password" name="signupFormPassword"/><br/>
                <label for="signupFormPasswordConfirmation">Mot de passe (confirmez) :</label><input id="signupFormPasswordConfirmation" type="password" name="signupFormPasswordConfirmation"/><br/>
                <button type="submit">Inscription</button>

            </fieldset>
        </form>
    {else}
        <p id="welcomeMessage">Bonjour, <strong>{$username }</strong>. <a href="logout.php">Déconnexion</a></p>
    {/if}
{/block}
{block name=body}
    {if $authenticated}
        <p><a href="pathologies.php">Accéder au catalogue des pathologies</a></p>
    {/if}
{/block}

