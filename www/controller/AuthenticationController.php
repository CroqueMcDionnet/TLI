<?php

require_once("model/Member.php");

class AuthenticationController {
    // login return codes
    const ALREADY_AUTHENTICATED = -2;
    const INVALID_CREDENTIALS = -1;
    const AUTHENTICATION_OK = 0;

    // signup return codes
    const UNKNOWN_ERROR = -4794;
    const USERNAME_ALREADY_REGISTERED = -4;
    const PASSWORDS_DO_NOT_MATCH = -3;
    const INVALID_FORMAT = -1;
    const REGISTER_OK = 0;

    private $webapp;

    public function __construct($webapp) {
        $this->webapp = $webapp;
    }

    public function hashPassword($plainTextPassword) {
        return password_hash($plainTextPassword, PASSWORD_BCRYPT);
    }

    public function verifyPassword($plainTextPassword, $hashedPassword) {
        return password_verify($plainTextPassword, $hashedPassword);
    }

    public function checkUsernameFormat($username) {
        // input is supposed to be sanitized already
        return (
            ctype_alnum($username) and
            strlen($username) >= 3 and
            strlen($username) <= 30
        );
    }

    public function checkPlainPasswordFormat($plainPassword) {
        // input is supposed to be sanitized already
        return (
        preg_match("/^[a-zA-Z0-9,.@%$!?_:;&-]{8,255}$/", $plainPassword)
        );
    }

    public function authenticate($username, $plainPassword) {
        if ($this->isAuthenticated()) {
            return self::ALREADY_AUTHENTICATED;
        }

        $pdo = $this->webapp->getPdo();
        $getUserStatement = $pdo->prepare('SELECT * FROM member WHERE username = ?');
        $getUserStatement->execute(Array($username));
        $getUserResult = $getUserStatement->fetch(PDO::FETCH_ASSOC);
        if (!isset($getUserResult['password'])) {
            return self::INVALID_CREDENTIALS;
        }

        $dbPassword = $getUserResult['password'];
        if (!password_verify($plainPassword, $dbPassword)) {
            return self::INVALID_CREDENTIALS;
        } else {
            $_SESSION['user'] = new Member($getUserResult['id'], $getUserResult['username'], $getUserResult['password']);
            return self::AUTHENTICATION_OK;
        }
    }

    public function register($username, $password, $passwordConfirmation) {
        if ($this->isAuthenticated()) {
            return self::ALREADY_AUTHENTICATED;
        }

        if (strcmp($password, $passwordConfirmation) !== 0) {
            return self::PASSWORDS_DO_NOT_MATCH;
        }

        if (!$this->checkUsernameFormat($username) or !$this->checkPlainPasswordFormat($password)) {
            return self::INVALID_FORMAT;
        }


        $pdo = $this->webapp->getPdo();
        $userExistsStatement = $pdo->prepare('SELECT COUNT(*) FROM member WHERE username = ?');
        $userExistsStatement->execute(Array($username));
        $userExists = $userExistsStatement->fetch(PDO::FETCH_NUM)[0] !== 0;

        if ($userExists) {
            return self::USERNAME_ALREADY_REGISTERED;
        }

        // ok for registration
        $createUserStatement = $pdo->prepare('INSERT INTO member (username, password) VALUES (?, ?)');
        if (!$createUserStatement->execute(Array($username, password_hash($password, PASSWORD_BCRYPT)))) {
            return self::UNKNOWN_ERROR;
        }

        $this->authenticate($username, $password);
        return self::REGISTER_OK;
    }

    public function isAuthenticated() {
        return isset($_SESSION['user']);
    }

    public function disconnect() {
        unset($_SESSION['user']);
    }

    public function getAuthenticatedUser() {
        return $_SESSION['user'];
    }
}