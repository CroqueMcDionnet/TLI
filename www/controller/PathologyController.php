<?php

class PathologyController {
    private $webapp;
    private $pathologyService;

    public function __construct($webapp, $pathologyService) {
        $this->webapp = $webapp;
        $this->pathologyService = $pathologyService;
    }

    public function findAll() {
        return $this->pathologyService->findAll();
    }
}