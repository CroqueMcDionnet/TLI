<?php
/* Smarty version 3.1.30, created on 2018-05-31 16:08:12
  from "/var/www/html/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b1001cc120070_77765754',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ac0d80c9f5195a4b1349fd7984fb80fa80c14534' => 
    array (
      0 => '/var/www/html/templates/index.tpl',
      1 => 1527775619,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout.tpl' => 1,
  ),
),false)) {
function content_5b1001cc120070_77765754 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10238998025b1001cc0ddca5_68026038', 'pageTitle');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18152154825b1001cc1185c6_00880105', 'mainAside');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8958022265b1001cc11e305_91023308', 'body');
?>


<?php $_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:layout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'pageTitle'} */
class Block_10238998025b1001cc0ddca5_68026038 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Accueil | Site Acupuncture<?php
}
}
/* {/block 'pageTitle'} */
/* {block 'mainAside'} */
class Block_18152154825b1001cc1185c6_00880105 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if ($_smarty_tpl->tpl_vars['loginOk']->value) {?><p id="loginOkMessage">Connexion réussie.</p><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['signupOk']->value) {?><p id="signupOkMessage">Inscription réussie. Vous êtes connecté.</p><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['disconnectOk']->value) {?><p id="loginOkMessage">Déconnexion réussie.</p><?php }?>
    <?php if (!$_smarty_tpl->tpl_vars['authenticated']->value) {?>
        <form action="/dologin.php" method="post">
            <fieldset>
                <legend>Connexion</legend>
                <?php if ($_smarty_tpl->tpl_vars['hasLoginError']->value) {?><p id="loginErrorMessage"><?php echo $_smarty_tpl->tpl_vars['loginErrorMessage']->value;?>
</p><?php }?>
                <label for="loginFormUsername">Nom d'utilisateur :</label><input id="loginFormUsername" type="text" name="loginFormUsername"/><br/>
                <label for="loginFormPassword">Mot de passe :</label><input id="loginFormPassword" type="password" name="loginFormPassword"/><br/>
                <button type="submit">Connexion</button>
            </fieldset>
        </form>
        <form action="/dosignup.php" method="post">
            <fieldset>
                <legend>Inscription</legend>
                <?php if ($_smarty_tpl->tpl_vars['hasSignupError']->value) {?><p id="signupErrorMessage"><?php echo $_smarty_tpl->tpl_vars['signupErrorMessage']->value;?>
</p><?php }?>
                <p id="signupFieldFormatHint">
                    Le nom d'utilisateur doit comporter de 3 à 30 caractères alphanumériques.<br/>
                    Le mot de passe doit comporter de 8 à 255 caractères alphanumériques et/ou spéciaux (,.@%$!?_:;&-).
                </p>
                <label for="signupFormUserame">Nom d'utilisateur :</label><input id="signupFormUsername" type="text" name="signupFormUsername"/><br/>
                <label for="signupFormPassword">Mot de passe :</label><input id="signupFormPassword" type="password" name="signupFormPassword"/><br/>
                <label for="signupFormPasswordConfirmation">Mot de passe (confirmez) :</label><input id="signupFormPasswordConfirmation" type="password" name="signupFormPasswordConfirmation"/><br/>
                <button type="submit">Inscription</button>

            </fieldset>
        </form>
    <?php } else { ?>
        <p id="welcomeMessage">Bonjour, <strong><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</strong>. <a href="logout.php">Déconnexion</a></p>
    <?php }
}
}
/* {/block 'mainAside'} */
/* {block 'body'} */
class Block_8958022265b1001cc11e305_91023308 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if ($_smarty_tpl->tpl_vars['authenticated']->value) {?>
        <p><a href="pathologies.php">Accéder au catalogue des pathologies</a></p>
    <?php }
}
}
/* {/block 'body'} */
}
