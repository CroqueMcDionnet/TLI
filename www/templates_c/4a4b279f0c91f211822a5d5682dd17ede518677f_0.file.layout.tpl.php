<?php
/* Smarty version 3.1.30, created on 2018-05-04 06:58:24
  from "C:\wamp\www\tli\www\templates\layout.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5aec04902e8e52_23571227',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4a4b279f0c91f211822a5d5682dd17ede518677f' => 
    array (
      0 => 'C:\\wamp\\www\\tli\\www\\templates\\layout.tpl',
      1 => 1525417103,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5aec04902e8e52_23571227 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <title><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_150535aec04902cf815_58438292', 'pageTitle');
?>
</title>
    <link rel="stylesheet" type="text/css" href="/tli/www/assets/css/main.css"/>
</head>
<body>
<header>
    <h1 id="siteTitle">Projet TLI | Acupuncture</h1>
    <hr/>
</header>
<aside id="mainAside">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_121625aec04902d6570_51165269', 'mainAside');
?>

</aside>
<main>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16835aec04902e1928_76603094', 'body');
?>

</main>
<footer>

</footer>
</body>
</html>
<?php }
/* {block 'pageTitle'} */
class Block_150535aec04902cf815_58438292 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Projet TLI | Acupuncture<?php
}
}
/* {/block 'pageTitle'} */
/* {block 'mainAside'} */
class Block_121625aec04902d6570_51165269 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'mainAside'} */
/* {block 'body'} */
class Block_16835aec04902e1928_76603094 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'body'} */
}
