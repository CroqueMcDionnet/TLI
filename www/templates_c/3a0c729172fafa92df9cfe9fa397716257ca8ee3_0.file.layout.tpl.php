<?php
/* Smarty version 3.1.30, created on 2018-05-31 14:07:00
  from "/var/www/html/templates/layout.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b10018441f206_50261801',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3a0c729172fafa92df9cfe9fa397716257ca8ee3' => 
    array (
      0 => '/var/www/html/templates/layout.tpl',
      1 => 1527775619,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b10018441f206_50261801 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <title><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11533301265b100184418201_20856927', 'pageTitle');
?>
</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css"/>
</head>
<body>
<header>
    <h1 id="siteTitle">Projet TLI | Acupuncture</h1>
    <hr/>
</header>
<aside id="mainAside">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18084939215b10018441aa83_70464052', 'mainAside');
?>

</aside>
<main>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8903588095b10018441d060_20768136', 'body');
?>

</main>
<footer>

</footer>
</body>
</html>
<?php }
/* {block 'pageTitle'} */
class Block_11533301265b100184418201_20856927 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Projet TLI | Acupuncture<?php
}
}
/* {/block 'pageTitle'} */
/* {block 'mainAside'} */
class Block_18084939215b10018441aa83_70464052 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'mainAside'} */
/* {block 'body'} */
class Block_8903588095b10018441d060_20768136 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'body'} */
}
