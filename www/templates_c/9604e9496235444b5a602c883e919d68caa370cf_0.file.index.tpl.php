<?php
/* Smarty version 3.1.30, created on 2018-05-04 07:03:45
  from "C:\wamp\www\tli\www\templates\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5aec05d1964c34_65633192',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9604e9496235444b5a602c883e919d68caa370cf' => 
    array (
      0 => 'C:\\wamp\\www\\tli\\www\\templates\\index.tpl',
      1 => 1525417424,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout.tpl' => 1,
  ),
),false)) {
function content_5aec05d1964c34_65633192 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_289895aec05d18b4834_95546742', 'pageTitle');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_320505aec05d194bb57_50260958', 'mainAside');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_149795aec05d195e677_24296836', 'body');
?>


<?php $_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:layout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'pageTitle'} */
class Block_289895aec05d18b4834_95546742 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Accueil | Site Acupuncture<?php
}
}
/* {/block 'pageTitle'} */
/* {block 'mainAside'} */
class Block_320505aec05d194bb57_50260958 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if ($_smarty_tpl->tpl_vars['loginOk']->value) {?><p id="loginOkMessage">Connexion réussie.</p><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['signupOk']->value) {?><p id="signupOkMessage">Inscription réussie. Vous êtes connecté.</p><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['disconnectOk']->value) {?><p id="loginOkMessage">Déconnexion réussie.</p><?php }?>
    <?php if (!$_smarty_tpl->tpl_vars['authenticated']->value) {?>
        <form action="/tli/www/dologin.php" method="post">
            <fieldset>
                <legend>Connexion</legend>
                <?php if ($_smarty_tpl->tpl_vars['hasLoginError']->value) {?><p id="loginErrorMessage"><?php echo $_smarty_tpl->tpl_vars['loginErrorMessage']->value;?>
</p><?php }?>
                <label for="loginFormUsername">Nom d'utilisateur :</label><input id="loginFormUsername" type="text" name="loginFormUsername"/><br/>
                <label for="loginFormPassword">Mot de passe :</label><input id="loginFormPassword" type="password" name="loginFormPassword"/><br/>
                <button type="submit">Connexion</button>
            </fieldset>
        </form>
        <form action="/tli/www/dosignup.php" method="post">
            <fieldset>
                <legend>Inscription</legend>
                <?php if ($_smarty_tpl->tpl_vars['hasSignupError']->value) {?><p id="signupErrorMessage"><?php echo $_smarty_tpl->tpl_vars['signupErrorMessage']->value;?>
</p><?php }?>
                <p id="signupFieldFormatHint">
                    Le nom d'utilisateur doit comporter de 3 à 30 caractères alphanumériques.<br/>
                    Le mot de passe doit comporter de 8 à 255 caractères alphanumériques et/ou spéciaux (,.@%$!?_:;&-).
                </p>
                <label for="signupFormUserame">Nom d'utilisateur :</label><input id="signupFormUsername" type="text" name="signupFormUsername"/><br/>
                <label for="signupFormPassword">Mot de passe :</label><input id="signupFormPassword" type="password" name="signupFormPassword"/><br/>
                <label for="signupFormPasswordConfirmation">Mot de passe (confirmez) :</label><input id="signupFormPasswordConfirmation" type="password" name="signupFormPasswordConfirmation"/><br/>
                <button type="submit">Inscription</button>

            </fieldset>
        </form>
    <?php } else { ?>
        <p id="welcomeMessage">Bonjour, <strong><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</strong>. <a href="logout.php">Déconnexion</a></p>
    <?php }
}
}
/* {/block 'mainAside'} */
/* {block 'body'} */
class Block_149795aec05d195e677_24296836 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if ($_smarty_tpl->tpl_vars['authenticated']->value) {?>
        <p><a href="pathologies.php">Accéder au catalogue des pathologies</a></p>
    <?php }
}
}
/* {/block 'body'} */
}
