<?php
require_once('Webapp.php');
session_start();

$webapp = new Webapp();
$config = $webapp->config;
$authenticationController = $webapp->getAuthenticationController();

$sanitizedUsername = filter_var($_POST['loginFormUsername'], FILTER_SANITIZE_STRING);
$sanitizedPassword = filter_var($_POST['loginFormPassword'], FILTER_SANITIZE_STRING);

switch($authenticationController->authenticate($sanitizedUsername, $sanitizedPassword)) {
    case AuthenticationController::ALREADY_AUTHENTICATED:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?loginError=alreadyAuthenticated');
        break;
    case AuthenticationController::INVALID_CREDENTIALS:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?loginError=invalidCredentials');
        break;
    case AuthenticationController::AUTHENTICATION_OK:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?loginOk');
        break;
    default:
        header('Location: ' . $config['root_url'] . $config['paths']['home'] . '?loginError=unknownError');
        break;
}

echo 'Redirection en cours... <a href="index.php">Cliquez ici si la page ne se charge pas.</a>';
die();